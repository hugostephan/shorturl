// ************
// * APP INIT *
// ************
// Requiring modules
const atob = require('atob');
const bodyParser = require('body-parser');
const btoa = require('btoa');
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const reader = require('fs');

// Read configuration file
let raw = reader.readFileSync("config.json");
var config = JSON.parse(raw);

// Init variables
var app = express();
var connectionString = config.options.database.connection_string ;
var port = config.options.server.listening_port ;
var promise ;

// Loading app modules and starting server
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({extended:true}));
var listener = app.listen(port, function() {
	console.log('Server started, listening on port : ' + listener.address().port);
});




// *********************
// * DATABASE SETTINGS *
// *********************
// Database Schema
var counterSchema = new mongoose.Schema({_id: {type:String, required: true},count: {type: Number, default: 0}});
var counter = mongoose.model(config.options.database.tables.counter, counterSchema);
var urlSchema = new mongoose.Schema({_id: {type: Number}, url: '', created_at:'', click: {type: Number, default: 0}});

// Pre saving 
urlSchema.pre('save', function(next) {
	//console.log('running pre-save');
	var doc = this;
	counter.findByIdAndUpdate({_id:'url_count'}, {$inc:{count: 1 } }, function(err, c) {
 		if(err) return next(err);
		doc._id = c.count;
		console.log(doc);
		doc.created_at = new Date();
		next();
    	});
});
var url = mongoose.model(config.options.database.tables.url, urlSchema);

// Database Connection
promise = mongoose.connect(connectionString, {
	useMongoClient: true
});

// Cleaning database
if(process.argv.indexOf("--resetdb") != -1 || process.argv.indexOf("-rdb") != -1)
{
	promise.then(function(db) {
		console.log('resetting database'),
		url.remove({}, function() {
			console.log('url collection removed');
		});
		counter.remove({}, function() {
			console.log('counter collection removed');
			var c = new counter({_id:'url_count', count: 10000});
			c.save(function(err) {
				if(err) return console.log(err);
				console.log('counter inserted');
			});
		});
	});
}




// **************
// * WEB ROUTES *
// **************
// Root
app.get('/', (req, res) => {
	res.redirect(config.options.server.default_redirect);
});

// Url shortening, POST /shorten
app.post('/shorten', function(req, res, next) {
	console.log(req.body.url);
	var urlData = decodeURIComponent(req.body.url) ;
	url.findOne({url: urlData}, function(err, doc) {
		if(doc) {
			console.log('entry found in db, url = ' + doc.url);
			res.send({
				url: urlData,
				hash: btoa(doc._id),
				status: 200,
				statusTxt: 'OK'
            		});
        	} else {
		    console.log('entry NOT found in db, saving new');
		    var u = new url({
		        url: urlData
		    });
		    u.save(function(err) {
		        if(err) return console.error(err);
			console.log("urlData = " +urlData);
		        res.send({
		            url: urlData,
		            hash: btoa(u._id),
		            status: 200,
		            statusTxt: 'OK'
		        });
		    });
		}
    	});
});

// Url redirect based on shorturl
app.get('/:hash', function(req, res) {
    var baseid = req.params.hash;
    var id = atob(baseid);
    url.findOne({ _id: id }, function(err, doc) {
        if(doc) {
		var newValue = doc.click+1;
		url.findByIdAndUpdate({_id:doc._id}, {$inc:{click: 1 } }, function(err, u) {
	 		if(err) return next(err);
			console.log(u);
	    	});

		url.update({_id: doc._id}, {click: newValue});

			// Not using too old links (> 14d old)
			var two_weeks = 1000 * 60 * 60 * 24 * config.options.server.max_days ;
			var two_weeks_time = new Date(new Date().getTime() - two_weeks);
			var created_at_time = new Date(doc.created_at);
			console.log(created_at_time);
			console.log(two_weeks_time);
			console.log(created_at_time > two_weeks_time);
			if(created_at_time > two_weeks_time) { res.redirect(doc.url); }
			else { console.log("too old"); res.redirect('/'); }
        } else {
        	res.redirect('/');
        }
    });
});