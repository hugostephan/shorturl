SHORTURL :
**********

Shorturl is a simple url shortener written with NodeJS and using MongoDB for data storage persistence.

It converts URL to hash.

Use POST /shorten, body: { url : yoururl} to shorten URL.
Use GET /:hash to be redirected to the URL corresponding to the hash.
